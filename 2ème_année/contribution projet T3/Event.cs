﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Event  {

    #region Attributs
      public string Nom {get; private set;}
      public string Description {get; private set;}
    #endregion

    /// <summary>  
    /// prototypes des autres méthodes 
    /// </summary>

    /// <summary>  
    /// Constructeur d'un event 
    /// </summary>

    public Event(string nom, string desc)
    {
        this.Nom = nom;
        this.Description = desc;
    }
}
