﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event_BuffEvent : Event {//fille de la classe Event

    #region Attributs
        public int Moral {get; set;}     // buf / debuf des caractéristiques en fonction de l'event (compris entre 0 et 100)
        public int Sante {get; set;}       
        public int Competence {get; set;}  
        public int Qui {get; private set;}
        public int Cible { get; set; }
    #endregion

    /* Constructeur de l'event qui affectera les caractéristiques des employés de l'entreprise */
    public Event_BuffEvent(string nom, int moral, int sante , int competence, string desc, int qui): base(nom, desc)
    {   
        this.Moral = moral;
        this.Sante = sante;
        this.Competence = competence;
        this.Qui = qui;
    }

}
