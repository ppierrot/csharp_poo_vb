using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Text;

#region Notes
//QUESTIONS
//comment ça s'articule entre les classes, qu'est ce qui fait office de "main" ? dans canevas
//comment ça fonctionne pour ajouter des évents dans Assets\Resources\Texte ?ok
//Ajouter un évent ça fait quoi dans le jeu concrètement ? interface (on s'en occupe pas)
//Comment faire le lien avec l'interface dans le jeu ? gaetan (on s'en occupe pas)
//Comment tester ? controler.cs 
//dans event_financier comment enlever la valeur dans somme à la rentabilité de l'entreprise ? Pareil pour event_buff mais avec les meployés. interface, appel de l'IA (on s'en occupe pas)
//INFOS
//productivite = ambition*competence
//fichier text eventbuff -> [0]nom, [1]moral, [2]santé, [3]compétence, [4]description, [5]touche tout le monde(1) ou un seul employé (0), [6] pondération pour contrôler les chances d'apparition de l'évent
//fichier text financier -> [0]nom, [1]effet sur rentabilité/productivité, [2]description
//PDG = GestionnaireEmp.lesEmpsDansEntreprise[0] et joueur = GestionnaireEmp.notreJoueur
#endregion

public static class GestionnaireEvent {

    #region Attributs
    public static string[] linebuff; //contient chaque lignes du fichier contenant les events buff employés
    public static string[] linefin; //contient chaque lignes du fichier contenant les events financiers

    public static int nbEventFin; //nombre d'events financiers
    public static int nbEventBuff; //nombre d'events buff employés

    public static object[,] tousLesEventBuff; // Liste de tous les event buff qui existent
    public static Queue<Event_Financier> tousLesEventFinancier = new Queue<Event_Financier>(); // Liste de tous les event financiers qui existent
    #endregion

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    /// <summary>
    /// Ajoute tous les events de buff dans une liste dédiée
    /// </summary>
    public static void AjouterTousLesEventBuff()
    {
        //On charge les données des events contenues dans les fichiers texte

        // dans fichier txt -> format ligne = nom;moral;sante;competence;description;1 ou all employés;pondération
        linebuff = Resources.Load<TextAsset>("Texte/eventBuffEmp").text.Split('\n');//enregistre chaque lignes séparément
      
        nbEventBuff = int.Parse(linebuff[0] as string); //1er ligne = nbr d'events
        tousLesEventBuff = new object[nbEventBuff,2];
        // Remplir la liste globale de tous les Events de buff du fichier texte
        string[] listToBuff;
      
        for(int i = 1; i < nbEventBuff; i++){
            listToBuff = linebuff[i].Split(';');//l'élément étant une ligne dont on va scinder le contenu qui sont séparés par des ';'
            tousLesEventBuff[i,0]=new Event_BuffEvent(listToBuff[0], int.Parse(listToBuff[1]), int.Parse(listToBuff[2]), int.Parse(listToBuff[3]), listToBuff[4], int.Parse(listToBuff[5])); //on créé un objet buff event avec comme paramètres chaque éléments scindés
            tousLesEventBuff[i,1] = int.Parse(listToBuff[6]);                     //TODO : Récupéré la pondération dans le fichier à la dernière case
        }
    }

  /// <summary>
  /// Ajoute tous les events financier dans une liste dédiée.
  /// </summary>

  public static void AjouterTousLesEventFinancier(){
  
  // Initialisation des variables
    tousLesEventFinancier.Clear(); //remmettre le tableau à 0

  //On charge les données des events contenues dans les fichiers texte
    // dans fichier txt -> format ligne = nom,effet,descritpion
    linefin = Resources.Load<TextAsset>("Texte/eventFinancier").text.Split('\n');
    nbEventFin = System.Convert.ToInt32(linefin[0]); //1er ligne = nbr d'events

    // Remplir la liste gloable de tous les Events de buff du fichier texte

    string[] listToFin;
      
      for(int i = 1; i < nbEventFin; i++){
                listToFin = linefin[i].Split(';');//l'élément étant une ligne dont on va scinder le contenu qui sont séparés par des ;
                tousLesEventFinancier.Enqueue(new Event_Financier(listToFin[0], listToFin[2], int.Parse(listToFin[1]))); //on créé un objet buff event avec comme paramètres chaque éléments scindés
            }
      }

    /// <summary>
    /// Fait varier dans un interval le bonus/malus attribué à un employé ou l'entreprise
    /// </summary>
    
    public static int VariationEvent(int stat){
      
     
      int interval = 0;
      
      //si la stat reçu est égale à 0 alors ça signifie qu'elle ne participe pas aux effets de l'évent 
      if (stat!=0)
      {
          interval = UnityEngine.Random.Range(-20, 21);
      }

      //calcul produit en croix et on rajoute le résultat à la stat
      stat += interval * stat / 100;

      return (stat);
    }

    /// <summary>
    /// On génère un nombre aléatoire entre 0 et 9 pour savoir si ce sera un évent de buff ou financier qui sere séléctionné
    /// </summary>

    public static bool TypeEvent(bool Buffevent){

        int interval = UnityEngine.Random.Range(0, 10);

        //pour l'instant on décide de rester sur que des events buff en attendant qu'on fasse le financier (qu'on implémente la rentabilité)
        if (interval<10){
            Buffevent = true;
        }

        return Buffevent;
    }

    /// <summary>
    /// Permet d'avoir directement l'event spécifique pour les vacances qui se passe tous les 12 tours
    /// </summary>

    public static Event_BuffEvent EventVac()
    {
        List<object[]> liste = new List<object[]>(); //nouvelle liste d'objets

        //on met dans la liste créé tous les évènements enregistrés dans tousLesEventBuff
        for (int i = 0; i < nbEventBuff; i++)
        {
            liste.Add(new object[] { tousLesEventBuff[i, 0], tousLesEventBuff[i, 1] });

        }
        //on créé un nouvel objet Event_BuffEvent avec les éléments du 9ème index de la liste 
        Event_BuffEvent ev = ((Event_BuffEvent)liste[8][1]);

        //on enregistre les valeurs amlus/bonus contenus dans chaque stats de l'event
        ev.Moral = VariationEvent(ev.Moral);
        int eventMoral = ev.Moral;

        ev.Competence = VariationEvent(ev.Competence);
        int eventComp = ev.Competence;

        ev.Sante = VariationEvent(ev.Sante);
        int eventSante = ev.Sante;

        //on définit l'attribut Cible de l'event à -1 pour dire que c'est tout le monde
        ev.Cible = -1;

        //tous les employés voient leurs stats changés avec les valeurs précédement enregistrées
        for (int i = 1; i < GestionnaireEmp.lesEmpsDansEntreprise.Count; i++)
        {
            // Prise d'effet sur le moral

            if (eventMoral > 0)
                GestionnaireEmp.lesEmpsDansEntreprise[i].Moral += (byte)(eventMoral);
            else
                GestionnaireEmp.lesEmpsDansEntreprise[i].Moral -= (byte)Mathf.Abs(eventMoral);

            // Prise d'effet sur la santé

            if (eventSante > 0)
                GestionnaireEmp.lesEmpsDansEntreprise[i].Sante += (byte)(eventSante);
            else
                GestionnaireEmp.lesEmpsDansEntreprise[i].Sante -= (byte)Mathf.Abs(eventSante);

            // Prise d'effet sur la compétence

            if (eventComp > 0)
                GestionnaireEmp.lesEmpsDansEntreprise[i].Competence += (byte)(eventComp);
            else
                GestionnaireEmp.lesEmpsDansEntreprise[i].Competence -= (byte)Mathf.Abs(eventComp);
        }

        return ev;
    }


    /// <summary>
    /// Choisir quel est le prochain event à apparaître et application de ses effets sur les employés/l'entreprise
    /// </summary>

    public static Event ProchainEvent(){

      Event EventNext=null;

      bool Buffevent = true; 
      TypeEvent(Buffevent);//on veut savoir si c'est un évent financier ou buff

        if (Buffevent == true) {
            int ponderation = 0; // poids total des events

            //on met tous les events buff dans une liste
            List<object[]> liste = new List<object[]>();
            for (int i = 0; i < nbEventBuff; i++)
            {
                liste.Add(new object[] { tousLesEventBuff[i, 0], tousLesEventBuff[i, 1] });
                ponderation += System.Convert.ToInt32(tousLesEventBuff[i, 1]);

            }
            int eventCible = UnityEngine.Random.Range(1, ponderation); // choix de l'event par rapport à la position dans la liste et au poids de l'event
            do
            {
                eventCible -= Convert.ToInt32(liste[0][1]);

                if (eventCible > 0)
                    liste.RemoveAt(0);

            } while (eventCible > 0);

            //on mélange la liste pour choisir un évent au hasard qui sera abitrairement (ça peut être autre chose si on veut) désigné dans liste[0] et on met les attributs sante, competence et moral dans des variables
            liste.MelangerUneListe();

            
            Event_BuffEvent ev = ((Event_BuffEvent)liste[0][0]);    //on assigne l'event choisis pour le return à la fin

            //on chope chaque attributs de l'évènement tiré dans une variable puis on lui applique une légère variation (+ ou - 20%) sur sa valeur 
            ev.Moral = VariationEvent (ev.Moral);
            int eventMoral = ev.Moral;


            ev.Competence = VariationEvent(ev.Competence);
            int eventComp = ev.Competence;

            ev.Sante = VariationEvent(ev.Sante);
            int eventSante = ev.Sante;


            //si l'attirbut 'Qui' est = 0 alors ça touche un seul employé sinon ça touche tous les employés
            if (ev.Qui==0){

                //on choisit au hasard un employé dans l'entreprise et on chope son index dans la liste des employés présent dans l'entreprise
                int pos = UnityEngine.Random.Range(0, GestionnaireEmp.lesEmpsDansEntreprise.Count);

                //on enregistre l'id (attibut qui appartient au type Emp) de l'employé dans l'attribut Cible de l'event séléctionné
                ev.Cible = GestionnaireEmp.lesEmpsDansEntreprise[pos].IdEmp;

                //

                //on applique les effets sur les stats de l'employé


                //si la variable intermédiaire est supérieure à 0, alors c'est un bonus, sinon c'est un malus

                if (eventMoral > 0)
                {
                    GestionnaireEmp.lesEmpsDansEntreprise[pos].Moral += (byte)(eventMoral);
                }
                else
                {
                    GestionnaireEmp.lesEmpsDansEntreprise[pos].Moral -= (byte)Mathf.Abs(eventMoral); //Mathf.Abs convertit la valeur en valeur absolue (soit non négative)
                }

                //on fait de même pour les autres attributs

                if (eventSante > 0)
                {
                    GestionnaireEmp.lesEmpsDansEntreprise[pos].Sante += (byte)(eventSante);
                }
                else
                {
                    GestionnaireEmp.lesEmpsDansEntreprise[pos].Sante -= (byte)Mathf.Abs(eventSante);
                }

                //le dernier ...

                if (eventComp > 0)
                    GestionnaireEmp.lesEmpsDansEntreprise[pos].Competence += (byte)(eventComp);
                else
                    GestionnaireEmp.lesEmpsDansEntreprise[pos].Competence -= (byte)Mathf.Abs(eventComp);
            }
            else{

                //on définit l'attribut Cible de BuffEvent à -1 pour dire que c'est tout le monde
                ev.Cible = -1;

                //tous les employés voient leurs stats changés comme ci-dessus
                for (int i = 1;i < GestionnaireEmp.lesEmpsDansEntreprise.Count;i++){

                    // Prise d'effet sur le moral
                    int SigneMoral = Convert.ToInt32(eventMoral);

                    if (SigneMoral > 0)
                        GestionnaireEmp.lesEmpsDansEntreprise[i].Moral += (byte)(SigneMoral);
                    else
                        GestionnaireEmp.lesEmpsDansEntreprise[i].Moral -= (byte)Mathf.Abs(SigneMoral);

                    // Prise d'effet sur la santé
                    int SigneSante = Convert.ToInt32(eventSante);

                    if (SigneSante > 0)
                        GestionnaireEmp.lesEmpsDansEntreprise[i].Sante += (byte)(SigneSante);
                    else
                        GestionnaireEmp.lesEmpsDansEntreprise[i].Sante -= (byte)Mathf.Abs(SigneSante);

                    // Prise d'effet sur la compétence
                    int SigneComp = Convert.ToInt32(eventComp);

                    if (SigneComp > 0)
                        GestionnaireEmp.lesEmpsDansEntreprise[i].Competence += (byte)(SigneComp);
                    else
                        GestionnaireEmp.lesEmpsDansEntreprise[i].Competence -= (byte)Mathf.Abs(SigneComp);
                }
            }
            EventNext = ev;
          }
      else{
        //**vide**
        //a définir comment on organise les events financiers, une fois la rentabilité implémenté : si ça touche tout le monde, quel attribut à changer,...
      }

      return EventNext;
    }  
}