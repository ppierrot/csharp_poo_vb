﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Event_Financier : Event{//fille de la classe Event

  #region Attributs
    public int Somme {get; private set;} // somme négative ou positive en fonction de l'event choisi, influera la productivité
  #endregion

  /// <summary>
  /// Constructeur de l'event financier qui affectera l'entreprise 
  /// </summary>

  public Event_Financier(string nom , string desc, int s): base(nom, desc)
  {
      this.Somme = s;
  }

}
