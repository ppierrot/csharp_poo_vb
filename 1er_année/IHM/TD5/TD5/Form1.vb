﻿Public Class Form1
    Private Sub TextBox_dep_MouseMove(sender As Object, e As MouseEventArgs) Handles TextBox_dep.MouseMove
        Dim depart As TextBox
        Dim resDragDrop As DragDropEffects
        If e.Button = MouseButtons.Left Then
            depart = CType(sender, TextBox)
            TextBox_exec.Text = "MouseMove"
            resDragDrop = depart.DoDragDrop(depart.Text, DragDropEffects.Move)
            If resDragDrop = DragDropEffects.Move Then
                depart.Clear()
                MsgBox("On vide")
            End If
            'MsgBox("Drag and Drop terminé")'
            MsgBox("Drag & Drop terminé")
        End If
    End Sub

    Private Sub TextBox_arr_DragEnter(sender As Object, e As DragEventArgs) Handles TextBox_arr.DragEnter
        'autorise le déplacement'
        If e.Data.GetDataPresent(DataFormats.Text) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub TextBox_arr_DragDrop(sender As Object, e As DragEventArgs) Handles TextBox_arr.DragDrop
        'cstr convertit'
        CType(sender, TextBox).Text = CStr(e.Data.GetData(DataFormats.Text))
        TextBox_exec.Text = "fin dragdrop"
    End Sub

    Private Sub EXO1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox_arr.AllowDrop = True
    End Sub
End Class
