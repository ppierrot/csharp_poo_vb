﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TextBox_dep = New System.Windows.Forms.TextBox()
        Me.TextBox_arr = New System.Windows.Forms.TextBox()
        Me.lbl_dep = New System.Windows.Forms.Label()
        Me.lbl_arr = New System.Windows.Forms.Label()
        Me.TextBox_exec = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'TextBox_dep
        '
        Me.TextBox_dep.Location = New System.Drawing.Point(12, 25)
        Me.TextBox_dep.Name = "TextBox_dep"
        Me.TextBox_dep.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_dep.TabIndex = 0
        '
        'TextBox_arr
        '
        Me.TextBox_arr.AllowDrop = True
        Me.TextBox_arr.Location = New System.Drawing.Point(12, 91)
        Me.TextBox_arr.Name = "TextBox_arr"
        Me.TextBox_arr.Size = New System.Drawing.Size(100, 20)
        Me.TextBox_arr.TabIndex = 1
        '
        'lbl_dep
        '
        Me.lbl_dep.AutoSize = True
        Me.lbl_dep.Location = New System.Drawing.Point(12, 9)
        Me.lbl_dep.Name = "lbl_dep"
        Me.lbl_dep.Size = New System.Drawing.Size(80, 13)
        Me.lbl_dep.TabIndex = 2
        Me.lbl_dep.Text = "Zone de départ"
        '
        'lbl_arr
        '
        Me.lbl_arr.AutoSize = True
        Me.lbl_arr.Location = New System.Drawing.Point(9, 75)
        Me.lbl_arr.Name = "lbl_arr"
        Me.lbl_arr.Size = New System.Drawing.Size(75, 13)
        Me.lbl_arr.TabIndex = 3
        Me.lbl_arr.Text = "Zone d'arrivée"
        '
        'TextBox_exec
        '
        Me.TextBox_exec.Location = New System.Drawing.Point(12, 146)
        Me.TextBox_exec.Name = "TextBox_exec"
        Me.TextBox_exec.Size = New System.Drawing.Size(168, 20)
        Me.TextBox_exec.TabIndex = 4
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(208, 178)
        Me.Controls.Add(Me.TextBox_exec)
        Me.Controls.Add(Me.lbl_arr)
        Me.Controls.Add(Me.lbl_dep)
        Me.Controls.Add(Me.TextBox_arr)
        Me.Controls.Add(Me.TextBox_dep)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox_dep As TextBox
    Friend WithEvents TextBox_arr As TextBox
    Friend WithEvents lbl_dep As Label
    Friend WithEvents lbl_arr As Label
    Friend WithEvents TextBox_exec As TextBox
End Class
