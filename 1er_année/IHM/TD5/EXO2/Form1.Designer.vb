﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TextBox_exec = New System.Windows.Forms.TextBox()
        Me.lbl_arr = New System.Windows.Forms.Label()
        Me.lbl_dep = New System.Windows.Forms.Label()
        Me.TextBox_arr = New System.Windows.Forms.TextBox()
        Me.TextBox_dep = New System.Windows.Forms.TextBox()
        Me.picbox_dep = New System.Windows.Forms.PictureBox()
        Me.picbox_arr = New System.Windows.Forms.PictureBox()
        CType(Me.picbox_dep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picbox_arr, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TextBox_exec
        '
        Me.TextBox_exec.Location = New System.Drawing.Point(12, 230)
        Me.TextBox_exec.Name = "TextBox_exec"
        Me.TextBox_exec.Size = New System.Drawing.Size(168, 20)
        Me.TextBox_exec.TabIndex = 9
        '
        'lbl_arr
        '
        Me.lbl_arr.AutoSize = True
        Me.lbl_arr.Location = New System.Drawing.Point(153, 9)
        Me.lbl_arr.Name = "lbl_arr"
        Me.lbl_arr.Size = New System.Drawing.Size(75, 13)
        Me.lbl_arr.TabIndex = 8
        Me.lbl_arr.Text = "Zone d'arrivée"
        '
        'lbl_dep
        '
        Me.lbl_dep.AutoSize = True
        Me.lbl_dep.Location = New System.Drawing.Point(9, 9)
        Me.lbl_dep.Name = "lbl_dep"
        Me.lbl_dep.Size = New System.Drawing.Size(80, 13)
        Me.lbl_dep.TabIndex = 7
        Me.lbl_dep.Text = "Zone de départ"
        '
        'TextBox_arr
        '
        Me.TextBox_arr.AllowDrop = True
        Me.TextBox_arr.Location = New System.Drawing.Point(156, 25)
        Me.TextBox_arr.Name = "TextBox_arr"
        Me.TextBox_arr.Size = New System.Drawing.Size(115, 20)
        Me.TextBox_arr.TabIndex = 6
        '
        'TextBox_dep
        '
        Me.TextBox_dep.Location = New System.Drawing.Point(12, 25)
        Me.TextBox_dep.Name = "TextBox_dep"
        Me.TextBox_dep.Size = New System.Drawing.Size(115, 20)
        Me.TextBox_dep.TabIndex = 5
        '
        'picbox_dep
        '
        Me.picbox_dep.Image = CType(resources.GetObject("picbox_dep.Image"), System.Drawing.Image)
        Me.picbox_dep.Location = New System.Drawing.Point(12, 51)
        Me.picbox_dep.Name = "picbox_dep"
        Me.picbox_dep.Size = New System.Drawing.Size(115, 115)
        Me.picbox_dep.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picbox_dep.TabIndex = 10
        Me.picbox_dep.TabStop = False
        '
        'picbox_arr
        '
        Me.picbox_arr.Location = New System.Drawing.Point(156, 51)
        Me.picbox_arr.Name = "picbox_arr"
        Me.picbox_arr.Size = New System.Drawing.Size(115, 115)
        Me.picbox_arr.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picbox_arr.TabIndex = 11
        Me.picbox_arr.TabStop = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(347, 262)
        Me.Controls.Add(Me.picbox_arr)
        Me.Controls.Add(Me.picbox_dep)
        Me.Controls.Add(Me.TextBox_exec)
        Me.Controls.Add(Me.lbl_arr)
        Me.Controls.Add(Me.lbl_dep)
        Me.Controls.Add(Me.TextBox_arr)
        Me.Controls.Add(Me.TextBox_dep)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.picbox_dep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picbox_arr, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox_exec As TextBox
    Friend WithEvents lbl_arr As Label
    Friend WithEvents lbl_dep As Label
    Friend WithEvents TextBox_arr As TextBox
    Friend WithEvents TextBox_dep As TextBox
    Friend WithEvents picbox_dep As PictureBox
    Friend WithEvents picbox_arr As PictureBox
End Class
