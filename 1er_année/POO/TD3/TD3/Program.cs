﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        

        static void Main(string[] args) 
        {
            int n = 99; //limite du futur tableau
            int[] tab = new int[n]; //création tableau
            Random randNum = new Random(); //appel méthode futur valeurs hasard
            for (int i = 0; i < n; i++) //entré de valeurs hasard dans le tableau
            {
                
                tab[i] = randNum.Next(0,500);
            }

            foreach (int i in tab) Console.WriteLine(i); //affichage des valeurs
            double average = tab.Average(); //moyenne valeurs tableau
            Console.WriteLine("La moyenne est : {0}", average);
            double max = tab.Max(); //max valeurs tableau
            Console.WriteLine("Le max : {0}", max);
            double min = tab.Min(); //min valeurs tableau
            Console.WriteLine("Le min : {0}", min);
            int som = tab.Sum(); //somme des valeurs du tableau
            Console.WriteLine("la somme est : {0}",som);
            Array.Sort(tab); //tri des valeurs tableau par odre croissant
            foreach (int i in tab) Console.WriteLine(i); //ré-affichage
            int x = 0; //variable qui contiendra valeur saisie par utilisateur
            bool ok = false; //flag (booléen) pour savoir si condition rempli
            Console.WriteLine("rentrez un entier à chercher dans le tableau");
            //test pour trouver valeur ( valeur entré par l'utilisateur) dans le tableau, gestion exception en anticipant utilisateur qui rentre lettre au lieu de chiffre (boucle while pour continuer à demander jusqu'à ce soit bon)
            while(!ok)
            try
                {
                    x = int.Parse (Console.ReadLine()); //Parse sert à convertir les string en int car les donnés saisies sont d'abord interprétées comme string
                    ok = true;
                }
            catch
                {
                    Console.WriteLine("vous devez rentrer un ENTIER");
                    ok = false;
                } 
            //si valeur de l'utilisateur présent ou pas, affiché selon le cas
            if (tab.Contains(x) == true)
            {
                Console.WriteLine("le tableau contient : {0}", x);
            }
            else
            {
                Console.WriteLine("le tableau ne contient PAS : {0}", x);
            }
            Console.ReadKey();
        }
    }
}
