﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDex
{
    class Program
    {
        static int Initlimit(int n) //fonction servant à définir la limite d'indexation des tableaux qui seront créé par la suite
        {
            Console.WriteLine("indiquez un nombre de valeurs maximum à rentrer");
            n = int.Parse(Console.ReadLine());
            return n;
        }
        static void Inittab(int n, int []tab) //on implémante les valeurs dans le tableau, en mettant en paramètre une limite définit plus tôt
        {
            for (int i = 1; i < n; i++)
            {
                Console.WriteLine("rentrez une valeur pour l'index {0} du tableau", i);
                tab[i] = int.Parse(Console.ReadLine());
            }
        }

        static int Schtroumpf(int []tab, int []tab2, int m, int n) //on multiplie chaque membre des deux tableaux entre eux et on additionne les résultats dans la variable somme, on retournera le résultat final
        {
            int somme = 0;
            for (int j = 1; j < m; j++)
            {
                for (int i = 1; 1 < n; i++)
                {
                    somme += tab[i] * tab2[j];
                }
            }

            Console.WriteLine(somme);
            return somme;
        }

        static void Main(string[] args)
        {
            //définition limite, création et remplissage du premier tableau
            int a = 0;
            Initlimit(a);
            int[] tab = new int[a]; 
            Inittab(a, tab);
            //définition limite, création et remplissage du deuxième tableau
            int b = 0;
            Initlimit(b);
            int[] tab2 = new int[b];
            Inittab(b, tab2);
            //multiplication des éléments du tableau entre eux et renvoi du résultat dans result
            int result = Schtroumpf(tab, tab2, a, b);
            //affichage final
            Console.WriteLine("le résultat est {0}",result);
            Console.ReadKey();


            //!!! le programme compile bien mais la fonction Inittab ne se met jamais en route
        }
    }
}
