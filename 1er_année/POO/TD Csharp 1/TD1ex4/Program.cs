﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD1ex4
{
    class Program
    {
        static void Main(string[] args)
        {
            string chaine;
            chaine = Console.ReadLine(); //on demande la saisie d'une chaine de caractères à l'utilisateur
            chaine.ToLower(); //passe lettres en minuscule
            chaine.Replace(" ", ""); //on remplace tous les espaces de la chaine pour que toutes les lettres soient collées
            char[] inverse = chaine.ToCharArray(); //on déclare une nouvelle variable de tableau caractères puis on la rempli de notre chaîne qu'on va scinder en caractères avec la fonction ToCharArray()
            Array.Reverse(inverse); //on inverse l'ordre du tableau de caractères contenus dans la variable passé en paramètre
            inverse.ToString(); //on convertir inverse en string
            if (inverse.Equals(chaine)) //dans une condition on vérifie que chaine soit = à inverse, si oui, alors c'est un palindrome, sinon ça ne l'est pas
            {
                Console.WriteLine("{0} est un palindrome", chaine);
            }
            else
            {
                Console.WriteLine("{0} n'est pas un palindrome", chaine);
            }
            Console.ReadKey();
            // !!! peu importe la chaine, le programme sort que ce n'est pas un palindrome -> problème de convertion de char à string ou problème avec la fonction .Equals() ?

        }
    }
}
