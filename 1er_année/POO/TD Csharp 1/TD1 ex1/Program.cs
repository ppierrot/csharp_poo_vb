﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD1
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 0; //variable servant à stocker âge de l'utilisateur, on attribut une valeur de départ quelconque
            bool ok = false; //flag (booléen) pour savoir si condition rempli
            //test pour trouver valeur ( valeur entré par l'utilisateur) dans le tableau, gestion exception en anticipant utilisateur qui rentre lettre au lieu de chiffre et/ou un chiffre au-dessus de 120, en-dessous de 0 (boucle while pour continuer à demander jusqu'à ce que les conditions soit remplies)
            while (!ok)
                try
                {
                    Console.WriteLine("Veuillez rentrez votre âge (compris entre 0 et 120)"); //méssage d'instruction pour l'utilisateur
                    age = int.Parse(Console.ReadLine()); //Parse sert à convertir les string en int car les donnés saisies sont d'abord interprétées comme string
                    if ((age > 0) && (age < 120))//condition pour voir si l'intervalle d'âge donné par l'utilisateur est bien comprise entre 0 et 120
                    {
                        ok = true; //toutes les conditions sont remplies, on sort de la boucle en mettant le flag sur true
                    }
                }
                catch
                {
                    Console.WriteLine("vous devez rentrer un ENTIER compris entre 0 et 120"); //méssage d'érreur pour prévenir l'utilisateur, puis nouvel éssai en retour de boucle
                }
            Console.WriteLine("vous êtes âgé de {0} ans.", age); //affichage âge de l'utilisateur 
            Console.ReadKey(); //là pour prévenir la fermeture brutale de la console à la fin de la compilation
        }
    }
}
