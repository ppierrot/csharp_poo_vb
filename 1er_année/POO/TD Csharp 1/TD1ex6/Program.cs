﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD1ex6
{
    class Program //on utilise la dichotomie (méthode de recherche en proposant le milieu d'un interval, renouvellant l'opération jusqu'à trouver le bon résultat)
    {
        static void Main(string[] args)
        {
            int a = 0; //variable représentant le minimat
            int z = 100; //variable représentant le maxima
            int i = 0; //variable compteur représentant nombre de tentatives
            string rep = ""; //variable de saisie pour l'utilisateur
            int prop = 50; //variable de proposition pour le nombre secret
            bool saisie = false; //flag répétant la demande de saisie jusqu'à proposition valide
            bool trouve = false; //flag répétant le système jusqu'à obtention du nombre secret

            while (! trouve) //tant que variable trouve est false
            {
                Console.WriteLine(prop); //affichage proposition
                saisie = false; //redémarrage boucle de saisie
               
                while (! saisie)
                {
                    Console.WriteLine("saisissez trop petit, trop grand ou gagné !");
                    rep = Console.ReadLine(); //demande de saisie utilisateur

                    if (rep == "gagné !")
                        {
                            saisie = true;
                            trouve = true;
                        }
                        else if (rep == "trop petit")
                        {
                            a = prop; //nouveau minimat
                            prop = (a+z)/2; // on reformule une nouvelle proposition adaptée 
                            i++; //incrémentation d'une tentative dans le compteur
                            saisie = true;
                        }
                        else if (rep == "trop grand")
                        {
                            z = prop; //nouveau maximat
                            prop = (a+z)/2; // on reformule une nouvelle proposition adaptée 
                            i++;//incrémentation d'une tentative dans le compteur
                            saisie = true;
                        }
                        else
                        {
                            Console.WriteLine("saisie invalide");
                        }
                }

            }
            Console.WriteLine("J'ai deviné le nombre secret en {0} coups.", i);
            Console.ReadKey();
        }
    }
}
