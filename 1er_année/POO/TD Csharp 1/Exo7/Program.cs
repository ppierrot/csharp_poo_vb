﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo7
{
    class Program
    {
        static void Main(string[] args)
        {
            // ATTENTION : programme bogué...

            int n; // nombre saisi
            int factorielle = 1; // factorielle de n (n!)

            Console.WriteLine("Entrez un entier > 0");
            n = int.Parse(Console.ReadLine());

            for (int i = 1; i < n+1; i++)
                factorielle = factorielle * i;

            Console.WriteLine("La factorielle de {0} vaut {1}", n, factorielle);
            Console.ReadLine();
        }
    }
}
