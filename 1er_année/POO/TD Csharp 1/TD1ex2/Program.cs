﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            int nombre=0; //variable servant à stocker le nombre à multiplier
            double multiple; //variable double (réel, nombre avec décimal) servant à stocker le résultat de la multiplication entre nombre et i
            Console.WriteLine("Entrez un nombre à multiplier jusqu'à 10");
            nombre = int.Parse(Console.ReadLine()); //l'utilisateur saisie la valeur de nombre
            for (int i = 0; i < 11; i++) //boucle pour opérer toutes les multiplications, ici, les 10 premières de la table définit par nombre
            {
                multiple = nombre * i; // on opère les multiples un par un
                Console.WriteLine("{0} x {1} = {2}", nombre, i, multiple); //affichage de l'opération et du résultat du multiple
            }
            Console.ReadKey();
        }
    }
}
