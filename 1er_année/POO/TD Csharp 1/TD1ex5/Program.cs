﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD1ex5
{
    class Program
    {
        static double Moyenne (params int[] tab) //la fonction admet plus d'un élément en paramètre, élément ensuite mis dans un tableau d'entiers, le résultat de la fonction sera renvoyé sous forme de réel
        {
            double moyenne = 0; //variable qui contiendra la moyenne
            foreach (int n in tab) //pour chaque éléments du tableau on va les ajouter dans moyenne
            {
                moyenne += tab[n]; //  !!! problème avec l'indexation de tab[] -> System.IndexOutOfRangeException : 'L'index se trouve en dehors des limites du tableau.'
            }
            moyenne /= tab.Length; //on divise moyenne par le nombre d'éléments contenu dans le tableau tab entré en paramètre de la fonction
            return moyenne;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(Moyenne(5,10,15)); //on affiche le résultat de la fonciton Moyenne pour les paramètres entrés
            Console.ReadKey();
            Console.WriteLine(Moyenne(45,58,5,6,3,2));  //on affiche le résultat de la fonciton Moyenne pour les paramètres entrés
            Console.ReadKey();
        }
        
    }
}
