﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TD1ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            double hab = 600; //le nombre d'habitants actuel de la ville
            int annee = 0; //le nombre d'années qui s'écouleront au fur et à mesure de l'accroissement de la population
            double taux = 5.02; //le taux d'augmentation en % de la ville chaques années
            int prevision = 10000; //l'objectif de futur population de la ville
            double stock = 0; //variable intermédiaire pour les calculs
            bool ok = false; //flag (booléen) pour savoir si condition rempli afin de quitter la boucle

            while (!ok) //on démare une boucle pour écouler les années et augmenter la population avec le taux jusqu'à ce que la population atteigne la valeur de prevision
            {
                if (hab < prevision)
                {
                    stock = (taux / 100) + 1; //par exemple, pour un taux de 0.89, on va le transformer en 1.089 à stocker dans la variable stock
                    hab *= stock; //on applique le taux d'augmentation en multipliant la population actuelle
                    annee++; //on incrémente une unité à l'écoulement des années
                }
                else
                {
                    ok = true; //une fois que la population a atteint la prevision, on sort de la boucle
                }
            }
            Console.WriteLine("La ville atteindra une population de {0} avec un taux d'acroissement de {1} dans {2} ans.", prevision, taux, annee); // affichage résultat
            Console.ReadKey();

        }
    }
}
