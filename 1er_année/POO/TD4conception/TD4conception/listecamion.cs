﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TD4conception
{
    public class Listecamion
    {
        List<Camion> l;
        string nomfich;
        public Listecamion (string n)
        {
            nomfich = n;
            l = new List<Camion>();
            StreamReader f = new StreamReader(nomfich);
            string str;
            while ((str = f.ReadLine())!=null)
            {
                string[] T;
                T = str.Split('_');
                Camion temp = new Camion(T[0], T[1], int.Parse(T[2]), int.Parse(T[3]), int.Parse(T[4]));
                l.Add(temp);
            }
            f.Close();
        }
        public void Affiche()
        {
            int i = 1;
            foreach(Camion c in l)
            {
                Console.Write(i + ":");
                c.Affiche;
                i++;
            }
        }

        public void Ajoute (string a, string b, int c, int d, int e)
        {
            Camion temp = new Camion(a, b, c, d, e);
            l.Add(temp);
        }

        public void Supprimer (int a)
        {
            if ((a>0)&&(a<=l.Count))
            {
                l.RemoveAt(a -1);
            }
            else
            {
                Console.WriteLine("indice est en dehors de la liste");
            }

        }
        
        
        public void Sort(int p)
        {
            foreach (Camion c in l)
            {
                if (c.Getprix() < p)
                    c.Affiche();
            }


        }

    }
}
