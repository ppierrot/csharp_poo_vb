﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net; // ajout
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TD2
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown(); // quitte l'application
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();  // nettoie la listbox
            string contenu = textBox.Text;  // recupère la donnée textbox
            IPHostEntry host = Dns.GetHostEntry(contenu);
            listBox.Items.Add(host);    // ajoute nom de l'hote dans la listbox
            foreach (IPAddress IP in host.AddressList)
            {
                listBox.Items.Add(host.AddressList);
            }
        }
    }
}
